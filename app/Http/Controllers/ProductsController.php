<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\Property;
use App\Models\Propertyvalue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('index', compact(['products']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(ProductRequest $request)
    {
        DB::transaction(function () use($request) {
            $varient = ($request->varient == "yes") ? 1:0;

            $imageAbsoluteUrl = '';
            if (isset($request->pic)) {
                $image = $request->pic->store('products');
                $imageAbsoluteUrl = asset('storage/' . $image);
            }

            $product = Product::create([
                'name' => $request->product_name,
                'image' => $imageAbsoluteUrl,
                'has_varients' => $varient,
                'price' => $request->product_price
            ]);

            if($varient && isset($request->SKU)) {
                $properties = [];
                foreach($request->property as $key => $value) {
                    $properties[] = Property::create([
                        'name' => $value
                    ]);
                }

                foreach($request->SKU as $varient) {
                    $sku = uniqid();
                    $productvarient = $product->varients()->create([
                        'sku' => $sku
                    ]);

                    $arr = explode("::",$varient);
                    foreach($arr as $key => $value) {
                        $productvarient->properties()->attach($properties[$key],['name' => $value]);
                    }
                }
            } else {
                $sku = uniqid();
                $product->varients()->create([
                    'sku' => $sku
                ]);
            }
        });
        return redirect(route('products.index'));
    }

    public function showVarients(Product $product) {
        $values = [];
        $properties = [];
        foreach($product->varients()->first()->properties as $property) {
            $properties[] = $property->name;
            foreach($property->productVarients as $varient) {
                $val = Propertyvalue::where(['productvarient_id' => $varient->id, 'property_id' => $property->id])->first()->name;
                $values[$varient->id][$property->name] = $val;
            }
        }
        dd($values);

        return view('varients', compact(['values', 'properties']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $properties = $product->varients()->first()->properties;

        $values = [];
        foreach($properties as $property) {
            foreach($property->productVarients as $varient) {
                $val = Propertyvalue::where(['productvarient_id' => $varient->id, 'property_id' => $property->id])->first()->name;
                if(!isset($values[$property->name]) || !in_array($val, $values[$property->name])) {
                    $values[$property->name][] = $val;
                }
            }
        }

        return view('edit', compact(['values', 'product']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        DB::transaction(function () use($request, &$product) {
            $varient = ($request->varient == "yes") ? 1:0;

            $imageAbsoluteUrl = '';

            if (!isset($request->pic)) {
                $imageAbsoluteUrl = $product->image;
            } else {
                $image = $request->pic->store('products');
                $imageAbsoluteUrl = asset('storage/' . $image);

                $product->deleteImage();
            }

            $product->update([
                'name' => $request->product_name,
                'image' => $imageAbsoluteUrl,
                'has_varients' => $varient,
                'price' => $request->product_price
            ]);

            if($varient && isset($request->SKU)) {

                $product->varients()->first()->properties()->detach();
                $properties = [];
                foreach($request->property as $key => $value) {
                    $properties[] = Property::create([
                        'name' => $value
                    ]);
                }

                $product->varients()->delete();
                foreach($request->SKU as $varient) {
                    $sku = uniqid();
                    $productvarient = $product->varients()->create([
                        'sku' => $sku
                    ]);

                    $arr = explode("::",$varient);
                    foreach($arr as $key => $value) {
                        $productvarient->properties()->attach($properties[$key],['name' => $value]);
                    }
                }
            } else {
                $sku = uniqid();
                $product->varients()->create([
                    'sku' => $sku
                ]);
            }
        });
        return redirect(route('products.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->back();
    }
}
