<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function varients() {
        return $this->hasMany(Productvarient::class);
    }

    public function deleteImage() {
        if($this->image != null) {
            $absolutePath = explode('/', $this->image);
            $count = count($absolutePath);
            $path = $absolutePath[$count - 2] . "/" . $absolutePath[$count - 1];
            Storage::delete($path);
        }
    }
}
