<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Property extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function uniqueProperty() {
    }
    public function productVarients() {
        return $this->belongsToMany(Productvarient::class)->withPivot('name');
    }
}
