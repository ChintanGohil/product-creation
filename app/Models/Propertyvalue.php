<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Propertyvalue extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = "productvarient_property";
}
