@extends('layout.app')


@section('styles')

@endsection


@section('content-section')
    <div class="mt-4 container">
        @include('partial._datatable')
    </div>
@endsection


@section('scripts')
    <script src="cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#product-table').DataTable();
        });
    </script>
@endsection
