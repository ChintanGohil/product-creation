@extends('layout.app')


@section('styles')
    <link rel="stylesheet" href="cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
@endsection


@section('content-section')
    <div class="mt-4 container">
        <div class="card card-default shadow">
            <div class="card-header card-header-border-bottom">
                <div class="d-flex justify-content-between">
                    <h5>Product List</h5>
                    <a href="{{route('products.index')}}" class="btn btn-primary">Go back</a>
                </div>
            </div>
            <div class="product-body p-3">
                <table id="product-table" class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>#</th>
                            @foreach ($properties as $value)
                                <th>{{$value}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($values as $key => $value)
                            <tr>
                                <th>{{$key}}</th>
                                @foreach ($properties as $value)
                                    <th>{{$values[$key][$value]}}</th>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#product-table').DataTable();
        });
    </script>
@endsection
