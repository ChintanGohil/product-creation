<div class="card card-default shadow">
    <div class="card-header card-header-border-bottom d-flex justify-content-between">
        <h5>Product List</h5>
        <a href="{{route('products.create')}}" class="btn btn-primary">+ Create Product</a>
    </div>
    <div class="product-body my-4">
        <table id="product-table" class="datatables-basic table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 1;
                    foreach ($products as $product) {
                ?>
                    <tr>
                        <td>{{$i++}}</td>
                        <td><img src="{{$product->image}}" alt="" class="product-image"></td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->price}}</td>
                        <td>
                            @if ($product->has_varients)
                                <a class="btn btn-success" href="{{route('product.varients',$product)}}">View Varients</a>
                            @endif
                            <a class="btn btn-primary" href="{{route('products.edit',$product)}}"><i class="fa fa-pencil"></i></a>
                            <form action="{{route('products.destroy', $product)}}" method="POST" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
