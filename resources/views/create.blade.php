@extends('layout.app')
@section('content-section')
            <div id="create-product" class="row">
                <div class="form-group">
                <form action="{{route('products.store')}}" id="product-form" class="col s8" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <p id="page-head">Create Product</p>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="d-flex justify-content-between">
                                    <label for="product_name" class="form-control">Product Name</label>
                                    <input type="text" id="product_name" class="form-control @error('product_name') is-invalid @enderror" name="product_name" value="{{old('product_name')}}">
                                </div>
                                @error('product_name')
                                    <small class="text-danger">{{$errors->first('product_name')}}</small>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <div class="d-flex justify-content-between">
                                    <label for="product_price" class="form-control">Product Price</label>
                                    <input type="number" id="product_price" class="form-control @error('product_price') is-invalid @enderror" name="product_price" value="{{old('product_price')}}">
                                </div>
                                    @error('product_price')
                                    <small class="text-danger">{{$errors->first('product_price')}}</small>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 my-4">
                        <div class="mb-3">
                            <label for="formFile" class="form-label">Choose Product Image</label>
                            <input class="form-control" type="file" id="formFile" name="pic" accept="image/*">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <h6>Does this product have any properties ? (size, color, style, etc)</h6>
                    </div>

                    <div class="col-md-12">
                        <div class="form-check">
                            <input class="form-check-input" type="radio"  name="varient" id="no-varients" value="no" >
                            <label class="form-check-label" for="no-varients">
                                No,this product does not have properties.
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="varient" id="has-varients" value="yes" checked>
                            <label class="form-check-label" for="has-varients">
                                Yes,this product has properties.
                            </label>
                          </div>
                    </div>

                    <div id="properties" class="my-4 card">
                        <div class="card-title p-3">
                            <h5>Property Details</h5>
                        </div>
                        <div class="card-body">
                            <div class="property-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <input type="text" placeholder="Property Name Eg.Color, Size" name="property[1]" class="property_name form-control" id="property_1">
                                            </div>
                                            <div class="col-md-7">
                                                <select class="form-control select" name="property_values_1[]" multiple="multiple">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a class="btn btn-primary" id="add-property">+ Add More Properties</a>
                        </div>
                    </div>

                    <div class="combination-section">
                        <button class="btn btn-primary" type="button" id="create-combinations">Create Combination</button>
                        <div class="text-danger combination-warning">Fill all the values first to create combination</div>
                    </div>

                    <div id="combination-tabl" class="col-md-12">

                            <table class="table">
                                <thead>
                                    <tr class="thead">

                                    </tr>
                                </thead>

                                <tbody class="tbody">

                                </tbody>
                            </table>
                    </div>

                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{route('products.index')}}" class="btn btn-primary">Cancel</a>
                        <span class="mt-4 presubmit-note"><br><strong>Note: </strong> Create combination before submitting the data</span>
                    </div>
                </form>
            </div>
    @endsection
    @section('scripts')
        <script>
            propertyCount = 2;
            $("#no-varients").click(function() {
                $("#properties").html("");
                $(".presubmit-note").toggleClass('d-none').toggleClass('d-block');
                $("#create-combinations").attr('disabled','disabled');
            });

            $("#has-varients").click(function() {
                $("#create-combinations").removeAttr('disabled');
                $(".presubmit-note").toggleClass('d-none').toggleClass('d-block');

                $("#properties").html(`
                    <div class="card-title p-3">
                        <h5>Property Details</h5>
                    </div>
                    <div class="card-body">
                        <div class="property-data">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <input type="text" placeholder="Property Name Eg.Color, Size" name="property[${propertyCount}]" class="property_name form-control" id="property_${propertyCount}">
                                        </div>
                                        <div class="col-md-7">
                                            <select class="form-control select" name="property_values_${propertyCount}[]" multiple="multiple">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-primary" id="add-property">+ Add More Properties</a>
                    </div>
                `);
                $(".select").select2({
                    tags: true,
                    tokenSeparators: [',', ' ']
                })
                propertyCount++;
            });

            $("#properties").on('click', '#add-property', function() {
                $("#properties .property-data").append(`
                    <div class="row my-3">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-5">
                                    <input type="text" placeholder="Property Name Eg.Color, Size" name="property[${propertyCount}]" class="property_name form-control" id="property_${propertyCount}">
                                </div>
                                <div class="col-md-7">
                                    <select class="form-control select" name="property_values_${propertyCount}[]" multiple="multiple">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                `);
                $(".select").select2({
                    tags: true,
                    tokenSeparators: [',', ' ']
                })
                propertyCount++;
            });
        </script>
        <script>
            $(".select").select2({
                tags: true,
                tokenSeparators: [',', ' ']
            })
        </script>

        <script>

            const combine = ([first, ...[second, ...third]]) => {
                if (!second) return first

                const combined = second.reduce((acc, x) => {
                    return acc.concat(first.map(h => `${h}::${x}`))
                }, []);

                return combine([combined, ...third]);
            };

            $("#create-combinations").click(function() {
                arr = Array();
                let i = 0;
                let is_empty = false;
                $(".select").each(function() {
                    if($(this).val().length == 0){
                        is_empty = true;
                    }
                    arr[i++] = $(this).val();
                });
                if(!is_empty) {
                    console.log(combine(arr));

                    insertData(combine(arr))
                    $('.combination-warning').addClass('d-none');
                } else {
                    $('.combination-warning').addClass('d-block');
                }
            });

            function insertData(arr) {
                propertyName = Array();

                let is_head_empty = false;
                let table_head_data = "";
                $('.property_name').each(function() {
                    if(($(this).val().length == 0)) {
                        is_head_empty = true;
                        return false;
                    }
                    table_head_data += `<th>${$(this).val()}</th>`;
                });

                if(!is_head_empty){
                    $(".thead").html('');
                    $(".tbody").html('');

                    $(".thead").append(table_head_data);

                    $(".thead").append("<th>Exclude</th>");

                    arr.forEach(element => {
                        subarr = element.split("::");
                        $data = "<tr>";

                        subarr.forEach(val => {
                            $data += `<td>${val}</td>`
                        });
                        $data += `
                            <td>
                                <p>
                                    <label>
                                        <input type="checkbox" checked="checked" class="form-check-input" name="SKU[]" value="${element}"/>
                                        <span></span>
                                    </label>
                                </p>
                            </td>
                        `;
                        $data += "</tr>";
                        $(".tbody").append($data);
                    });
                } else {
                    arr = Array();
                }
            }
        </script>
@endsection
